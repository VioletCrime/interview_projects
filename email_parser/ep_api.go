package main

import (
    "encoding/json"
    "fmt"
    "io/ioutil"
    "log"
    "net/http"

    "email_parser/eputil"
    "github.com/gorilla/mux"
)

func ParseEmailEndpoint(w http.ResponseWriter, req *http.Request) {
    body, err := ioutil.ReadAll(req.Body)
    if err != nil {
        log.Printf("%v\n", err)
        w.WriteHeader(http.StatusInternalServerError)
        w.Write([]byte(fmt.Sprintf("Failed to parse request body: %v", err)))
        return
    }
    w.WriteHeader(http.StatusOK)
    email_string := string(body)
    result := eputil.ParseEmail(&email_string)
    fmt.Printf("\nRESULT: %+v\n", *result)
    json.NewEncoder(w).Encode(*result)
}

func main() {
    router := mux.NewRouter()
    router.HandleFunc("/parse", ParseEmailEndpoint).Methods("POST")
    log.Fatal(http.ListenAndServe(":8080", router))
}
