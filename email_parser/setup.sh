#!/bin/bash

set -e

readonly PROGNAME=$(basename $0)
readonly PROGDIR=$(readlink -m $(dirname $0))
readonly ARGS="$@"

usage(){
    cat <<- EOF
Usage: $PROGNAME OPTIONS...

Ex.
  ./$PROGNAME -t          # Run tests
  ./$PROGNAME -b -c       # Build and containerize binary
  ./$PROGNAME -d -p 1234  # Run docker container binding to host port 1234

Options:
  -h, --help            Show this message and exit
  -a, --all             Test, Build, Containerize, and Deploy
  -t, --test            Run the tests
  -b, --build           Build the binaries
  -c, --containerize    Build Docker images using binaries
  -d, --deploy          Deploy Docker images
  -p, --port            Host port to bind service to
EOF
}

parse_args(){
    options=`getopt -o hatbcdp: --long help,all,test,build,containerize,deploy,port: -n "$PROGNAME" -- "$@"`
    eval set -- $options
    while true
    do
        case $1 in
            -h|--help)
                usage ; exit 0 ;;
            -a|--all)
                TEST=true ; BUILD=true ; DOCKERIZE=true ; DEPLOY=true ; shift ;;
            -t|--test)
                TEST=true ; shift ;;
            -b|--build)
                BUILD=true ; shift ;;
            -c|--containerize)
                DOCKERIZE=true ; shift ;;
            -d|--deploy)
                DEPLOY=true ; shift ;;
            -p|--port)
                PORT=$2 ; shift 2 ;;
            --)
                shift ; break ;;
        esac
    done
    if ! $TEST && ! $BUILD && ! $DOCKERIZE && ! $DEPLOY; then
        usage
        exit 1
    fi
}

main(){
    local TEST=false
    local BUILD=false
    local DOCKERIZE=false
    local DEPLOY=false
    local PORT=8080

    parse_args $ARGS

    pushd .
    WORKSPACE=`pwd`/..
    WORKSPACE=`readlink -f $WORKSPACE`  # Evaluate the '/..' from above to get parent directory
    if $TEST; then
        run_tests
    fi
    if $BUILD; then
        build
    fi
    if $DOCKERIZE; then
        dockerize
    fi
    if $DEPLOY; then
        deploy
    fi
    popd
}

run_tests(){
    echo "=====  Running Tests  ================================="
    run_golang_docker_cmd "cd /go/src/email_parser/eputil && go test"
}

build(){
    echo "=====  Building Bins  ================================="
    run_golang_docker_cmd "cd /go/src/email_parser && go get github.com/gorilla/mux && CGO_ENABLED=0 GOOS=linux go build -a -o ep_api"
}

dockerize(){
    echo "=====  Building Docker Image  ========================="
    cp ep_api ep_api_docker
    docker build -t ep_api ep_api_docker
}

deploy(){
    echo "=====  Running Docker Image  =========================="
    echo "Example request using curl:"
    echo "    curl -X POST --data-binary @$PROGDIR/eputil/test_email.txt localhost:$PORT/parse"
    docker run -p $PORT:8080 ep_api
}

run_golang_docker_cmd(){
    CMD=$1
    run_docker_cmd "golang:1.9" "$CMD"
}

run_docker_cmd(){
    IMAGE=$1
    CMD=$2
    set -x
    docker run -v $WORKSPACE:/go/src $IMAGE bash -c "$CMD"
    set +x
}
main
