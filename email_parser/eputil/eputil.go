package eputil

import (
    "strings"
)

type Email struct {
    To string `json:"to"`
    From string `json:"from"`
    Date string `json:"date"`
    Subject string `json:"subject"`
    MessageID string `json:"message-id"`
}

func ParseEmail(email *string) *Email {
    result := &Email{}
    lines := strings.Split(*email, "\n")
    for _, line := range lines {
        if strings.HasPrefix(line, "To: ") {
            result.To = strings.Split(line, ": ")[1]
        } else if strings.HasPrefix(line, "From: ") {
            result.From = strings.Split(line, ": ")[1]
        } else if strings.HasPrefix(line, "Date: ") {
            result.Date = strings.Split(line, ": ")[1]
        } else if strings.HasPrefix(line, "Subject: ") {
            result.Subject = strings.Split(line, ": ")[1]
        } else if strings.HasPrefix(line, "Message-ID: ") {
            result.MessageID = strings.Split(line, ": ")[1]
        }
    }
    return result
}

