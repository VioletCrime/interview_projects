package eputil

import (
    "fmt"
    "io/ioutil"
    "testing"
)

func TestParseUtil(t *testing.T) {
    expected_to := `Kyle Smith <kyle.smith.2501@gmail.com>`
    expected_from := `Senorita Redacted <notification@jobvite.com>`
    expected_date := `Thu, 7 Dec 2017 17:07:47 +0000`
    expected_subj := `Phone Interview at Redacted for Software Engineer`
    expected_mid := `<0100016031f28caa-6d3a2029-a162-46e4-ab9e-b9b0d8cfe800-000000@email.amazonses.com>`
    test_email, err := ioutil.ReadFile("./test_email.txt")
    if err != nil {
        t.Error(err)
    }
    test_email_string := string(test_email)
    result := ParseEmail(&test_email_string)
    if result.To != expected_to {
        t.Error(fmt.Sprintf("(TO) Expected: %v, Got: %v\n", expected_to, result.To))
    }
    if result.From != expected_from {
        t.Error(fmt.Sprintf("(FROM) Expected: %v, Got: %v\n", expected_from, result.From))
    }
    if result.Date != expected_date {
        t.Error(fmt.Sprintf("(DATE) Expected: %v, Got: %v\n", expected_date, result.Date))
    }
    if result.Subject != expected_subj {
        t.Error(fmt.Sprintf("(SUBJECT) Expected: %v, Got: %v\n", expected_subj, result.Subject))
    }
    if result.MessageID != expected_mid {
        t.Error(fmt.Sprintf("(MSG_ID) Expected: %v, Got: %v\n", expected_mid, result.MessageID))
    }
}

