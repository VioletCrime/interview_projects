package main

import (
    "flag"
    "fmt"
    "os"

    "create_weather_histogram/cwhutil"
)

func main() {
    // Define and read user inputs
    file := flag.String("file", "", "Log file to compile expected weather information for. *REQUIRED*")
    outfile := flag.String("outfile", "", "Output file to write results to. *REQUIRED*")
    bucket_count := flag.Int("buckets", 5, "Number of histogram buckets to create.")
    cache := flag.Bool("cache", false, "Use cached information")
    dumpcache := flag.Bool("dumpcache", false, "Dump the cache to stdout")
    cpuprofile := flag.String("cpuprofile", "", "File to write pprof results to")
    flag.Parse()

    //flag.PrintDefaults()
    if *dumpcache {
        cwhutil.DumpCache()
    } else if *file == "" {
        fmt.Fprintln(os.Stderr, "ERROR: 'file' argument must be specified")
        flag.PrintDefaults()
    } else if *outfile == "" {
        fmt.Fprintln(os.Stderr, "ERROR: 'outfile' argument must be specified")
        flag.PrintDefaults()
    } else {
        cwhutil.Run(file, *outfile, *bucket_count, *cache, *cpuprofile)
    }
}
