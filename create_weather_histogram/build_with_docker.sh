#!/bin/bash

set -ex

readonly PROGNAME=$(basename $0)
readonly PROGDIR=$(readlink -m $(dirname $0))
readonly ARGS="$@"

pushd .
cd $PROGDIR

BIN_NAME="CreateWeatherHistogram"

WORKSPACE=`git rev-parse --show-toplevel`/..
WORKSPACE=`readlink -f $WORKSPACE`  # Evaluate the '/..' from above to get parent directory
CMD="cd /go/src/create_weather_histogram && go build -o $BIN_NAME"

docker run -v $WORKSPACE:/go/src golang:1.9 bash -c "$CMD"

popd
