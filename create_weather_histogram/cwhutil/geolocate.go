package cwhutil

import (
    "bytes"
    "encoding/json"
    "fmt"
    "net/http"
)

type geolocEntry struct {
    Query string
    Lat float64
    Lon float64
}


// First, generate a list of IPs which need to be geolocated (cache may be current
// for some / all). Then, for every (up to) 50 ips that need to be geolocated, start
// a goroutine to fetch geolocation data. Finally, call waitForJobs.
func geolocateIps() {
    // Create a slice of IPs that require geolocation data; may not be all of them
    data_map := getDataMap()
    var ips []string
    for ip := range *data_map {
        if geoDataIsTooOld(ip) {
            ips = append(ips, ip)
        }
    }

    // Create and populate the buffered channel used to signal routines to start work
    semaphore := make(chan bool, goroutine_limit)
    for i := 0; i < int(goroutine_limit); i++ {
        semaphore <- true
    }

    // Max batch request size is 50 IPs; create a goroutine for each 50-ip slice
    start := 0
    end := 50
    done := make(chan bool, 1)
    routine_count := 0
    for start < len(ips) {
        if end > len(ips) {
            end = len(ips)
        }
        go fetchGeolocData(ips[start:end], done, semaphore)
        routine_count++
        start += 50
        end += 50
    }
    waitForJobs(done, semaphore, routine_count)
}

// Given a list of (up to) 50 IPs, build a request to ip-api.com, and update
// the data_map appropriately with the response.
func fetchGeolocData(ips []string, done chan<- bool, semaphore <-chan bool) {
    defer func() {
        if e := recover(); e != nil {
            addGeolocError(fmt.Sprintf("%v", e))
            done <- true
        }
    }()
    <- semaphore  // Wait for signal to go

    // Build request body
    url := "http://ip-api.com/batch"
    var body []map[string]string
    for _, ip := range ips {
        body_entry := make(map[string]string)
        body_entry["query"] = ip
        body_entry["fields"] = "query,lat,lon"
        body = append(body, body_entry)
    }
    body_json, err := json.Marshal(body)
    if err != nil { panic(err) }

    // Build and send request
    client := &http.Client{}
    request, req_err := http.NewRequest("POST", url, bytes.NewBuffer(body_json))
    if req_err != nil { panic(req_err) }
    request.Close = true
    resp, err := client.Do(request)
    if err != nil { panic(err) }
    defer resp.Body.Close()

    // Handle response
    geoloc_response := make([]geolocEntry,0)
    err = json.NewDecoder(resp.Body).Decode(&geoloc_response)
    if err != nil { panic(err) }
    for _, response := range geoloc_response {
        getDataMap().setGeoLoc(response.Query, response.Lat, response.Lon)
    }
    done <- true  // Alert routine complete
}

// Wait for each routine to indicate it has concluded
func waitForJobs(done <-chan bool, semaphore chan<- bool, routine_count int) {
    prog := createProgressBar("Getting geolocation data", routine_count)
    for i := 0; i < routine_count; i++ {
        prog.outputProgress(i)
        <- done
        semaphore <- true
    }
    prog.complete()
}
