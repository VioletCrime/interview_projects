package cwhutil

import (
    "testing"
)

func TestProcessData(t *testing.T) {
    wipeDataMap()
    for i := range test_datum {
        addToDataMap(&test_datum[i])
    }
    temps, max, min, b_size := processData(4)

    if len(*temps) != 8 {
        t.Error("ProcessData() should have identified 8 unique temperatures")
    }
    if max != 80 {
        t.Error("ProcessData() did not correctly identify the max temp as 80")
    }
    if min != -7 {
        t.Error("ProcessData() did not correctly identify the min temp as -7")
    }
    if b_size != 21.75 {
        t.Error("ProcessData() did not correctly identiry the bucket size as 21.75")
    }
}

func TestGenerateHistogram(t *testing.T) {
    wipeDataMap()
    for i := range test_datum {
        addToDataMap(&test_datum[i])
    }
    hist := generateHistogram(4)

    if hist.buckets[0].min != -7 {
        t.Error("GenerateHistogram() configured a bucket incorrectly")
    }
    if hist.buckets[0].max != 14.75 {
        t.Error("GenerateHistogram() configured a bucket incorrectly")
    }
    if hist.buckets[0].count != 12 {
        t.Error("GenerateHistogram() configured a bucket incorrectly")
    }

    if hist.buckets[1].min != 14.75 {
        t.Error("GenerateHistogram() configured a bucket incorrectly")
    }
    if hist.buckets[1].max != 36.5 {
        t.Error("GenerateHistogram() configured a bucket incorrectly")
    }
    if hist.buckets[1].count != 9 {
        t.Error("GenerateHistogram() configured a bucket incorrectly")
    }

    if hist.buckets[2].min != 36.5 {
        t.Error("GenerateHistogram() configured a bucket incorrectly")
    }
    if hist.buckets[2].max != 58.25 {
        t.Error("GenerateHistogram() configured a bucket incorrectly")
    }
    if hist.buckets[2].count != 7 {
        t.Error("GenerateHistogram() configured a bucket incorrectly")
    }

    if hist.buckets[3].min != 58.25 {
        t.Error("GenerateHistogram() configured a bucket incorrectly")
    }
    if hist.buckets[3].max != 80 {
        t.Error("GenerateHistogram() configured a bucket incorrectly")
    }
    if hist.buckets[3].count != 24 {
        t.Error("GenerateHistogram() configured a bucket incorrectly")
    }
}
