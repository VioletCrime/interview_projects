package cwhutil

import (
    "fmt"
    "testing"
    "time"
)

func TestSaveAndLoad(t *testing.T) {
    data_map := getDataMap()
    for i := range test_datum {
        addToDataMap(&test_datum[i])
    }
    saveCache()
    defer wipeCacheFile()
    wipeDataMap()
    data_map, err := loadCache()
    if err != nil {
        t.Error(err)
    }
    for _, data := range test_datum {
        _, exists := (*data_map)[data.ip]
        if !exists {
            t.Error(fmt.Sprintf("An IP (%v) does not exist in the data structure!", data.ip))
        }
        if (*data_map)[data.ip].count != 0 {
            t.Error("An IPs count was restored, but should not have been")
        }
        if (*data_map)[data.ip].Latitude != data.lat {
            t.Error(fmt.Sprintf("An IP has the incorrect latitude (expected: %v, got %v)", data.lat, (*data_map)[data.ip].Latitude))
        }
        if (*data_map)[data.ip].Longitude != data.lon {
            t.Error(fmt.Sprintf("An IP has the incorrect longitude (expected: %v, got %v)", data.lon, (*data_map)[data.ip].Longitude))
        }
        if (*data_map)[data.ip].High_temp != data.temp {
            t.Error(fmt.Sprintf("An IP has the incorrect temperature (expected: %v, got %v)", data.temp, (*data_map)[data.ip].High_temp))
        }
    }
}

func TestSaveAndApply(t *testing.T) {
    data_map := getDataMap()
    for i := range test_datum {
        addToDataMap(&test_datum[i])
    }
    saveCache()
    defer wipeCacheFile()
    wipeDataMap()
    data_map = getDataMap()
    for i := range test_datum {
        for j := 0; j < test_datum[i].count; j++ {
            data_map.addIp(test_datum[i].ip)
        }
    }
    applyCache()
    for _, data := range test_datum {
        _, exists := (*data_map)[data.ip]
        if !exists {
            t.Error(fmt.Sprintf("An IP (%v) does not exist in the data structure!", data.ip))
        }
        if (*data_map)[data.ip].count != data.count {
            t.Error(fmt.Sprintf("An IP does not have the expected count (expected: %v, got: %v)", data.count, (*data_map)[data.ip].count))
        }
        if (*data_map)[data.ip].Latitude != data.lat {
            t.Error(fmt.Sprintf("An IP has the incorrect latitude (expected: %v, got %v)", data.lat, (*data_map)[data.ip].Latitude))
        }
        if (*data_map)[data.ip].Longitude != data.lon {
            t.Error(fmt.Sprintf("An IP has the incorrect longitude (expected: %v, got %v)", data.lon, (*data_map)[data.ip].Longitude))
        }
        if (*data_map)[data.ip].High_temp != data.temp {
            t.Error(fmt.Sprintf("An IP has the incorrect temperature (expected: %v, got %v)", data.temp, (*data_map)[data.ip].High_temp))
        }
    }
}

func TestApplyCacheIgnoresOldData(t *testing.T) {
    dat := test_datum[0]
    addToDataMap(&dat)
    data_map := getDataMap()
    (*data_map)[dat.ip].Geo_data_fetched = time.Now().Add(-168 * time.Hour)
    (*data_map)[dat.ip].Temp_data_fetched = time.Now().Add(-168 * time.Hour)
    saveCache()
    defer wipeCacheFile()
    wipeDataMap()
    data_map = getDataMap()
    data_map.addIp(dat.ip)
    applyCache()
    if (*data_map)[dat.ip].Latitude != 0 {
        t.Error("Cache applied old latitude data when it should not have")
    }
    if (*data_map)[dat.ip].Longitude != 0 {
        t.Error("Cache applied old longitude data when it should not have")
    }
    if (*data_map)[dat.ip].High_temp != zeroFaren {
        t.Error("Cache applied old temperature data when it should not have")
    }
}

func TestMultiSaveCacheAppendsNotOverwrites(t *testing.T) {
    dat1 := test_datum[0]
    dat2 := test_datum[1]
    dat3 := test_datum[2]

    addToDataMap(&dat1)
    saveCache()
    defer wipeCacheFile()
    wipeDataMap()

    addToDataMap(&dat2)
    saveCache()
    wipeDataMap()

    addToDataMap(&dat3)
    saveCache()
    wipeDataMap()

    data_map := getDataMap()
    data_map.addIp(dat1.ip)
    data_map.addIp(dat2.ip)
    data_map.addIp(dat3.ip)
    applyCache()

    if (*data_map)[dat1.ip].Latitude != dat1.lat {
        t.Error("Cached data was not reapplied (lost?)")
    }
    if (*data_map)[dat1.ip].Longitude != dat1.lon {
        t.Error("Cached data was not reapplied (lost?)")
    }
    if (*data_map)[dat1.ip].High_temp != dat1.temp {
        t.Error("Cached data was not reapplied (lost?)")
    }
    if (*data_map)[dat2.ip].Latitude != dat2.lat {
        t.Error("Cached data was not reapplied (lost?)")
    }
    if (*data_map)[dat2.ip].Longitude != dat2.lon {
        t.Error("Cached data was not reapplied (lost?)")
    }
    if (*data_map)[dat2.ip].High_temp != dat2.temp {
        t.Error("Cached data was not reapplied (lost?)")
    }
    if (*data_map)[dat3.ip].Latitude != dat3.lat {
        t.Error("Cached data was not reapplied (lost?)")
    }
    if (*data_map)[dat3.ip].Longitude != dat3.lon {
        t.Error("Cached data was not reapplied (lost?)")
    }
    if (*data_map)[dat3.ip].High_temp != dat3.temp {
        t.Error("Cached data was not reapplied (lost?)")
    }
}

func TestCacheLifespanFunctions(t *testing.T) {
    data_map := getDataMap()
    dat := test_datum[0]
    data_map.addIp(dat.ip)
    data_map.setGeoLoc(dat.ip, dat.lat, dat.lon)
    data_map.setHighTemp(dat.ip, dat.temp, false)

    if geoDataIsTooOld(dat.ip) {
        t.Error("Fresh geolocation data is incorrectly being reported as 'too old'")
    }
    if tempDataIsTooOld(dat.ip) {
        t.Error("Fresh weather data is incorrectly being reported as 'too old'")
    }

    (*data_map)[dat.ip].Geo_data_fetched = time.Now().Add(-cache_lifespan).Add(-1 * time.Minute)
    (*data_map)[dat.ip].Temp_data_fetched = time.Now().Add(-cache_lifespan).Add(-1 * time.Minute)

    if !geoDataIsTooOld(dat.ip) {
        t.Error("Old geolocation data is incorrectly being reported as 'fresh enough'")
    }
    if !tempDataIsTooOld(dat.ip) {
        t.Error("Old weather data is incorrectly being reported as 'fresh enough'")
    }
}
