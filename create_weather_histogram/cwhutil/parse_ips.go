package cwhutil

import (
    "fmt"
    "io/ioutil"
    "regexp"
    "strings"
    "sync"
)


// Top-level logic for the workflow. Read in the log, parse IPs, and
// generate the map we'll use for the remainder of the process
func compileDataMap(file *string) {
    log_data := readLog(file)
    ips := isolateIps(log_data)
    addIpsToDataMap(ips)
}


// Simply open the file specified by the user, and return the string
// representation of its contents
func readLog(file *string) *string {
    // Read in the log file
    data_raw, err := ioutil.ReadFile(*file)
    checkErr(err, "Error occurred reading input data from file", true)
    data := string(data_raw)
    return &data
}


// Using a pointer to a string, find all valid IPs in the string, returning
// the regex.FindAllString() output
func isolateIps(data *string) *[]string {
    oct_regex := `\b(25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[0-9]?[0-9])\b`
    ip_pattern := oct_regex + "\\." + oct_regex + "\\." + oct_regex + "\\." + oct_regex
    regex := regexp.MustCompile(ip_pattern)
    result := regex.FindAllString(*data, -1)
    return &result
}


// Using the slice of ips, generate an [ip]entry map
func addIpsToDataMap(data *[]string) {
    data_map := getDataMap()
    private_ip_count := 0
    for _, ip := range *data {
        data_map.addIp(ip)
        if ipInPrivateRange(ip) {
            private_ip_count += 1
            getDataMap().setGeoLoc(ip, 0, 0)
            if tempDataIsTooOld(ip) {
                getDataMap().setHighTemp(ip, getPrivateIpTemp(ip), false)
            }
        }
    }
    fmt.Printf("IP count (unique/total/private): %v / %v / %v (%v)\n", len(*data_map), len(*data), private_ip_count, private_ip_temp_str)
}

func ipInPrivateRange(ip string) bool {
    result := strings.HasPrefix(ip, "192.168.") || strings.HasPrefix(ip, "10.")  // Check for 192.168. and 10.
    if !result {
        for i := 16; i < 32; i++ { // Check for 172.16/12 address space
            prefix := fmt.Sprintf("172.%v.", i)
            if strings.HasPrefix(ip, prefix) {
                result = true
                break
            }
        }
    }
    return result
}

var private_ip_temp int
var private_ip_fetch sync.Once
var private_ip_temp_str = ""

func getPrivateIpTemp(ip string) (temp int) {
    defer func() {
        if e := recover(); e != nil {
            private_ip_temp = getRandomTemp()
            temp = private_ip_temp
            private_ip_temp_str = fmt.Sprintf("%vF*", temp)
        }
    }()
    private_ip_fetch.Do(func() {  // Only lookup the 0/0 temp once
        private_ip_temp = fetchTemp(ip)
    })
    temp = private_ip_temp
    private_ip_temp_str = fmt.Sprintf("%vF", temp)
    return
}