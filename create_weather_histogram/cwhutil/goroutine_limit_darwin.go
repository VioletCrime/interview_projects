package cwhutil

// Since Darwin is BSD, I'll assume it has similar limitation to what I've observed on
// Linux, and play it way safe...
func setRoutineLimit() {
    goroutine_limit = 256
}
