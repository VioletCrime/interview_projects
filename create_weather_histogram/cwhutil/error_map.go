package cwhutil

import (
    "fmt"
    "sync"
)

type error_map_type map[string]int
var error_map_lock = sync.RWMutex{}

var error_map = &error_map_type{}

func addGeolocError(err string) {
    addError(fmt.Sprintf("GEOLOC: '%v'", err))
}

func addForecastError(err string) {
    addError(fmt.Sprintf("FORECAST: '%v'", err))
}

func addError(err string) {
    error_map_lock.Lock()
    defer error_map_lock.Unlock()
    _, ok := (*error_map)[err]
    if ok {
        (*error_map)[err] += 1
    } else {
        (*error_map)[err] = 1
    }
}

func printErrors() {
    error_map_lock.RLock()
    defer error_map_lock.RUnlock()
    if len(*error_map) > 0 {
        fmt.Println("\nERRORS:")
    }
    for k, v := range *error_map {
        fmt.Printf("   %v - seen %v times\n", k, v)
    }
}

