package cwhutil

import (
    "fmt"
    "testing"
)

func getTestStr(ip string) string {
    return fmt.Sprintf("asdf %v qwerty", ip)
}

func TestIsolateIps(t *testing.T) {
    ip := "1.2.3.4"
    test_str := getTestStr(ip)
    result := isolateIps(&test_str)
    if len(*result) != 1 {
        t.Error(fmt.Sprintf("isolateIps() failed to parse a valid IP (%v)", ip))
    }

    ip = "1.2.3.256"  // Should not truncate to 1.2.3.25
    test_str = getTestStr(ip)
    result = isolateIps(&test_str)
    if len(*result) > 0 {
        t.Error(fmt.Sprintf("isolateIps() parsed an invalid IP (%v)", ip))
    }

    ip = "1.2.256.4"
    test_str = getTestStr(ip)
    result = isolateIps(&test_str)
    if len(*result) > 0 {
        t.Error(fmt.Sprintf("isolateIps() parsed an invalid IP (%v)", ip))
    }

    ip = "1.256.3.4"
    test_str = getTestStr(ip)
    result = isolateIps(&test_str)
    if len(*result) > 0 {
        t.Error(fmt.Sprintf("isolateIps() parsed an invalid IP (%v)", ip))
    }

    ip = "256.2.3.4"  // Should not truncate to 56.2.3.4
    test_str = getTestStr(ip)
    result = isolateIps(&test_str)
    if len(*result) > 0 {
        t.Error(fmt.Sprintf("isolateIps() parsed an invalid IP (%v)", ip))
    }

    ip = "255.255.255.255"
    test_str = getTestStr(ip)
    result = isolateIps(&test_str)
    if len(*result) != 1 {
        t.Error(fmt.Sprintf("isolateIps() failed to parse a valid IP (%v)", ip))
    }

    ip = "0.0.0.0"
    test_str = getTestStr(ip)
    result = isolateIps(&test_str)
    if len(*result) != 1 {
        t.Error(fmt.Sprintf("isolateIps() failed to parse a valid IP (%v)", ip))
    }

    // KNOWN ISSUE - this will parse two IPs- 1.1.2.3 and 5.8.13.21
    //test_str = "1.1.2.3.5.8.13.21.34.55.89"
    //result = isolateIps(&test_str)
    //if len(*result) > 0 {
    //    t.Error(fmt.Sprintf("isolateIps() parsed an invalid IP (%v): %v", ip, result))
    //}
}

func TestAddIpsToDataMap(t *testing.T) {
    test := []string{"1.2.3.4", "2.3.4.5", "3.4.5.6", "4.5.6.7"}
    addIpsToDataMap(&test)
    data_map := getDataMap()
    for _, ip := range test {
        _, exists := (*data_map)[ip]
        if !exists {
            t.Error(fmt.Sprintf("addIpsToDataMap() failed to add IP '%v'", ip))
        }
    }
}
