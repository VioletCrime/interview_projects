package cwhutil

// https://blogs.technet.microsoft.com/markrussinovich/2009/09/29/pushing-the-limits-of-windows-handles/
// This could be WAY larger, but this is as far as I care to push it.
func setRoutineLimit() {
    goroutine_limit = 20000
}