package cwhutil

import (
    "fmt"
    "strings"
)

type progress struct {
    process string
    total int
    iterated bool
}

func (p *progress) outputProgress(count int) {
    if p.total == 0 {
        return
    }
    p.iterated = true
    num_boxes := (count * 20) / p.total
    num_blank := 20 - num_boxes
    prog_bar := fmt.Sprintf("[%v%v]", strings.Repeat("#", num_boxes), strings.Repeat(" ", num_blank))
    percent := (count * 100) / p.total
    counter := fmt.Sprintf("(%v/%v)", count, p.total)
    fmt.Printf("\r%v: %v %v%% %v", p.process, prog_bar, percent, counter)
}

func (p *progress) complete() {
    if p.total == 0 || !p.iterated {
        return
    }
    p.outputProgress(p.total)
    fmt.Println("")
}

func createProgressBar(process string, total int) *progress {
    result := progress{process, total, false}
    return &result
}
