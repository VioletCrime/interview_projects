package cwhutil

import (
    "encoding/json"
    "errors"
    "fmt"
    "math/rand"
    "net/http"
    "time"
)

var randomized_temps int
var rando = rand.New(rand.NewSource(time.Now().UnixNano()))

type response struct {
    Success bool
    Error struct {
        Code string
        Description string
    }
    Response []struct {
        Periods []struct {
            Timestamp int64
            MaxTempF int
        }
    }
}

func getForecasts() {
    randomized_temps = 0

    // Create and populate channel used to limit concurrently-running goroutines
    semaphore := make(chan bool, goroutine_limit)
    for i := 0; i < int(goroutine_limit); i++ {
        semaphore <- true
    }

    // Goroutines populate this channel when their operations complete
    done := make(chan bool, 1)

    // Create all goroutines
    data_map := getDataMap()
    routine_count := 0
    for ip := range *data_map {
        if tempDataIsTooOld(ip) {
            go updateTempData(ip, done, semaphore)
            routine_count++
        }
    }

    // Monitor goroutines, populating semaphore as routines complete
    prog := createProgressBar("Getting forecast data", routine_count)
    for i := 0; i < routine_count; i++ {
        prog.outputProgress(i)
        <- done
        semaphore <- true
    }
    prog.complete()

    // If any data was faked, inform the user
    if randomized_temps > 0 {
        fmt.Printf("Generated %v random temperatures as a result of failures fetching weather data\n", randomized_temps)
    }
}

func updateTempData(ip string, done chan<- bool, semaphore <-chan bool) {
    defer func() {
        if e := recover(); e != nil {
            // Note the error we're recovering from
            addForecastError(fmt.Sprintf("%v", e))
            // Assume we didn't get valid data- fake some
            getDataMap().setHighTemp(ip, getRandomTemp(), true)
            randomized_temps++
            done <- true  // Alert goroutine complete
        }
    }()
    <- semaphore  // Wait for clearance, Clarence
    getDataMap().setHighTemp(ip, fetchTemp(ip), false)  // Fetch temp data, update data_map accordingly
    done <- true  // Alert goroutine complete
}

func fetchTemp(ip string) int {
    // Build URL
    lat, lon := getLatLon(ip)
    id, secret := getCreds()
    url := fmt.Sprintf("http://api.aerisapi.com/forecasts/%v,%v?filter=day&limit=2&fields=periods.timestamp,periods.maxTempF&client_id=%v&client_secret=%v", lat, lon, id, secret)

    // Build and send request
    client := &http.Client{}
    request, req_err := http.NewRequest("GET", url, nil)
    if req_err != nil { panic(req_err) }
    request.Close = true
    resp, err := client.Do(request)
    if err != nil { panic(err) }
    defer resp.Body.Close()

    // Handle response appropriately
    if resp.StatusCode != 200 {  // Even non-successes return 200
        msg := fmt.Sprintf("Non-200 error code returned from weather API (%v)", resp.StatusCode)
        panic(errors.New(msg))
    }
    var response response
    err = json.NewDecoder(resp.Body).Decode(&response)
    if err != nil { panic(err) }
    if !response.Success {  // If the success field of the response is false, note the error
        panic(errors.New(fmt.Sprintf("%v: %v", response.Error.Code, response.Error.Description)))
    }
    return response.Response[0].Periods[1].MaxTempF
}

func getRandomTemp() int {
    return rando.Intn(100)
}

func getCreds() (string, string) {
    id := ""
    secret := ""
    return id, secret
}
