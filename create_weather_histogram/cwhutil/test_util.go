package cwhutil

import (
    "os"
)

var test_ip = "1.2.3.4"
var test_lat = -76.54
var test_lon = 45.87
var test_temp = 42

type test_data struct {
    ip string
    count int
    lat float64
    lon float64
    temp int
}

var td1 = test_data{"1.1.1.1", 3, 78.0, 42.12, 78}
var td2 = test_data{"2.2.2.2", 4, -34.46, -20.82, -7}
var td3 = test_data{"3.3.3.3", 6, 36.4, -91.68, 80}
var td4 = test_data{"4.4.4.4", 10, -7.3, 15.76, 75}
var td5 = test_data{"5.5.5.5", 9, 1.03, -65.98, 24}
var td6 = test_data{"6.6.6.6", 7, -118.75, -95.7, 53}
var td7 = test_data{"7.7.7.7", 4, -132.6, -131.72, 75}
var td8 = test_data{"8.8.8.8", 1, 51.9, 141.76, 77}
var td9 = test_data{"9.9.9.9", 8, -17.14, 151.85, 9}
var test_datum = []test_data{td1, td2, td3, td4, td5, td6, td7, td8, td9}

func wipeDataMap() {
    map_instance = &data_map{}
}

func wipeCacheFile() {
    os.Remove(cachefile)
}

func addToDataMap(dat *test_data) {
    data_map := getDataMap()
    for i := 0; i < dat.count; i++ {
        data_map.addIp(dat.ip)
    }
    data_map.setGeoLoc(dat.ip, dat.lat, dat.lon)
    data_map.setHighTemp(dat.ip, dat.temp, false)
}
