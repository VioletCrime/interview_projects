package cwhutil

import (
    "syscall"
)

func setRoutineLimit() {
    var fd_limit syscall.Rlimit
    fd_err := syscall.Getrlimit(syscall.RLIMIT_NOFILE, &fd_limit)
    if fd_err != nil {
        checkErr(fd_err, "Error encountered identifying file descriptor limit (linux-specific)", false)
        goroutine_limit = 512  // Ubuntu default is 1024 limit
    } else {
        goroutine_limit =  fd_limit.Cur / 2  // Both resp.Body and request consume a fd
    }
}