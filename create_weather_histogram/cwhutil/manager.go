package cwhutil

import (
    "errors"
    "fmt"
    "os"
    "runtime/pprof"
)

var goroutine_limit uint64

func checkErr(e error, msg string, fatal bool) {
    if e != nil {
        full_msg := fmt.Sprintf("%v: '%v'", msg, e)
        fmt.Printf("%v\n", full_msg)
        if fatal {
            panic(e)
        }
    }
}

func Run(infile *string, outfile string, bucket_count int, cache bool, profile string) {
    // Sanity-check inputs
    if bucket_count < 1 {
        e := errors.New("ERROR: Histogram bucket count cannot be less than '1'!")
        checkErr(e, "", true)
    }

    // CPU profiling setup
    if profile != "" {
        prof_f, err := os.Create(profile)
        if err != nil {
            checkErr(err, "Error encountered creating cpu profile file", false)
        } else {
            pprof.StartCPUProfile(prof_f)
            defer pprof.StopCPUProfile()
        }
    }

    // Identify how many file descriptors we can use for tcp sockets
    setRoutineLimit()
    fmt.Printf("Set goroutine_limit to %v\n", goroutine_limit)

    // Create a data map from the input file
    compileDataMap(infile)

    // Update data map with previously-discovered geoloc & temp values if appropriate
    if cache {
        applyCache()
    }

    // Figure out lat/long for each IP in the data map
    geolocateIps()
    getForecasts()
    printErrors()
    saveCache()  // Aways save the cache
    writeHistogram(outfile, bucket_count)
}
