package cwhutil

import (
    "encoding/gob"
    "fmt"
    "os"
    "strings"
    "time"
)

const cachefile = ".cwh.cache"
const cache_lifespan = 6 * time.Hour

func saveCache() {
    dataMap := getDataMap()
    cache, err := loadCache()
    if err != nil {
        cache = &data_map{}
    }

    map_lock.RLock()
    for ip := range *dataMap {
        _, ok := (*cache)[ip]
        if ok {
            if (*dataMap)[ip].Geo_data_fetched.After((*cache)[ip].Geo_data_fetched) {
                (*cache)[ip].Latitude = (*dataMap)[ip].Latitude
                (*cache)[ip].Longitude = (*dataMap)[ip].Longitude
                (*cache)[ip].Geo_data_fetched = (*dataMap)[ip].Geo_data_fetched
            }
            if (*dataMap)[ip].Temp_data_fetched.After((*cache)[ip].Temp_data_fetched) {
                (*cache)[ip].High_temp = (*dataMap)[ip].High_temp
                (*cache)[ip].Temp_data_fetched = (*dataMap)[ip].Temp_data_fetched
            }
        } else {
            (*cache)[ip] = (*dataMap)[ip]
        }
    }
    map_lock.RUnlock()

    var enc_err error
    file, err := os.Create(cachefile)
    checkErr(err, fmt.Sprintf("Error opening / creating the cache file '%v'", err), false)
    defer file.Close()
    if err == nil {
        encoder := gob.NewEncoder(file)
        enc_err = encoder.Encode(*cache)
    }
    checkErr(enc_err, "Error encrypting data for caching", false)
}

func loadCache() (*data_map, error) {
    var cache data_map
    var d_err error
    file, f_err := os.Open(cachefile)
    defer file.Close()
    if f_err == nil {
        decoder := gob.NewDecoder(file)
        d_err = decoder.Decode(&cache)
    } else {
        if !strings.Contains(f_err.Error(), "no such file or directory") {
            checkErr(f_err, "Error encountered while opening cachefile", false)
        }
        return nil, f_err
    }
    checkErr(d_err, "Error encountered during cache decode", false)
    return &cache, d_err
}

func applyCache() {
    datamap := getDataMap()
    cache, err := loadCache()
    if err == nil {
        map_lock.Lock()
        for ip := range *datamap {
            _, exists := (*cache)[ip]
            if exists {
                if !dataNeedsUpdating((*cache)[ip].Geo_data_fetched) {
                    (*datamap)[ip].Latitude = (*cache)[ip].Latitude
                    (*datamap)[ip].Longitude = (*cache)[ip].Longitude
                    (*datamap)[ip].Geo_data_fetched = (*cache)[ip].Geo_data_fetched
                }
                if !dataNeedsUpdating((*cache)[ip].Temp_data_fetched) {
                    (*datamap)[ip].High_temp = (*cache)[ip].High_temp
                    (*datamap)[ip].Temp_data_fetched = (*cache)[ip].Temp_data_fetched
                }
            }
        }
        map_lock.Unlock()
    }
}

func DumpCache() {
    cache, err := loadCache()
    if err == nil {
        for k, v := range *cache {
            fmt.Printf("%v\n    %v\n", k, v)
        }
    }
}

func geoDataIsTooOld(ip string) bool {
    return dataNeedsUpdating(getGeoTimestamp(ip))
}

func tempDataIsTooOld(ip string) bool {
    return dataNeedsUpdating(getTempTimestamp(ip))
}

func dataNeedsUpdating(then time.Time) bool {
    return then.Add(cache_lifespan).Before(time.Now())
}

