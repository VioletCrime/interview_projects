package cwhutil

import (
    "fmt"
    "testing"
    "time"
)

func TestMapSingleton(t *testing.T) {
    wipeDataMap()
    ptr_one := getDataMap()
    ptr_two := getDataMap()

    if ptr_one != ptr_two {
        t.Error("getDataMap() is returning different addresses for 'map_instance")
    }
}

func TestAddIp(t *testing.T) {
    wipeDataMap()
    data_map := getDataMap()
    data_map.addIp(test_ip)
    _, exists := (*data_map)[test_ip]
    if !exists {
        t.Error("addIp() failed to add an IP key to the map")
    }
}

func TestStructInitState(t *testing.T) {
    wipeDataMap()
    data_map := getDataMap()
    data_map.addIp(test_ip)
    if (*data_map)[test_ip].count != 1 {
        t.Error("ip_entry struct count should be initialized as '1'")
    }
    if (*data_map)[test_ip].Latitude != 0 {
        t.Error("ip_entry struct latitude should be initialized as '0'")
    }
    if (*data_map)[test_ip].Longitude != 0 {
        t.Error("ip_entry struct longitude should be initialized as '0'")
    }
    if !(*data_map)[test_ip].Geo_data_fetched.Equal(time.Unix(0, 0)) {
        t.Error("ip_entry struct geodata timestamp should be initialized as Unix time 0")
    }
    if (*data_map)[test_ip].High_temp != zeroFaren {
        t.Error("ip_entry struct temperature should be initialized as zeroFaren")
    }
    if !(*data_map)[test_ip].Temp_data_fetched.Equal(time.Unix(0, 0)) {
        t.Error("ip_entry struct forecast timestamp should be initialized as Unix time 0")
    }
}

func TestSetGeoLoc(t *testing.T) {
    wipeDataMap()
    now := time.Now()
    data_map := getDataMap()
    data_map.addIp(test_ip)
    data_map.setGeoLoc(test_ip, test_lat, test_lon)
    if (*data_map)[test_ip].Latitude != test_lat {
        t.Error("setGeoLoc() failed to set the correct latitude")
    }
    if (*data_map)[test_ip].Longitude != test_lon {
        t.Error("setGeoLoc() failed to set the correct longitude")
    }
    if now.After((*data_map)[test_ip].Geo_data_fetched) {
        t.Error("setGeoLoc() failed to set the geodata timestamp")
    }
}

func TestSetHighTempRealData(t *testing.T) {
    wipeDataMap()
    now := time.Now()
    data_map := getDataMap()
    data_map.addIp(test_ip)
    data_map.setHighTemp(test_ip, test_temp, false)
    if (*data_map)[test_ip].High_temp != test_temp {
        t.Error("setHighTemp() failed to set the forecasted temperature")
    }
    if now.After((*data_map)[test_ip].Temp_data_fetched) {
        t.Error("setHighTemp() failed to set the forecast timestamp")
    }
}

func TestSetHighTempFakeData(t *testing.T) {
    wipeDataMap()
    data_map := getDataMap()
    data_map.addIp(test_ip)
    data_map.setHighTemp(test_ip, test_temp, true)
    if (*data_map)[test_ip].High_temp != test_temp {
        t.Error("setHighTemp() failed to set the forecasted temperature")
    }
    if !time.Unix(0, 0).Equal((*data_map)[test_ip].Temp_data_fetched) {
        t.Error("setHighTemp() updated the timestamp on faked data")
    }
}

func TestGetLatLon(t *testing.T) {  // Tests getLatitude(), getLongitude() and getLatLon()
    wipeDataMap()
    data_map := getDataMap()
    data_map.addIp(test_ip)
    data_map.setGeoLoc(test_ip, test_lat, test_lon)
    la, lo := getLatLon(test_ip)
    if la != test_lat {
        t.Error("getLatLon() returned incorrect latitude")
    }
    if lo != test_lon {
        t.Error("getLanLon() returned incorrect longitude")
    }
}

func TestGetTemp(t *testing.T) {
    wipeDataMap()
    data_map := getDataMap()
    data_map.addIp(test_ip)
    data_map.setHighTemp(test_ip, test_temp, false)
    if getTemp(test_ip) != test_temp {
        t.Error("getTemp() returned incorrect temp")
    }
}

func TestGetGeoTimestamp(t *testing.T) {
    wipeDataMap()
    data_map := getDataMap()
    data_map.addIp(test_ip)
    tnow := time.Now()
    data_map.setGeoLoc(test_ip, test_lat, test_lon)
    if getGeoTimestamp(test_ip).Before(tnow) {
        t.Error("getGeoTimestamp() returned incorrect time")
    }
}

func TestGetTempTimestamp(t *testing.T) {
    wipeDataMap()
    data_map := getDataMap()
    data_map.addIp(test_ip)
    tnow := time.Now()
    data_map.setHighTemp(test_ip, test_temp, false)
    if getTempTimestamp(test_ip).Before(tnow) {
        t.Error("getTempTimestamp() returned incorrect time")
    }
}

func TestDataMapIsThreadSafe(t *testing.T) {
    wipeDataMap()
    done := make(chan bool, 1)
    for i := range test_datum {
        go populate_data(&test_datum[i], done)
    }
    for i := 0; i < len(test_datum); i++ {
        <- done
    }
    data_map := getDataMap()
    for _, data := range test_datum {
        _, exists := (*data_map)[data.ip]
        if !exists {
            t.Error(fmt.Sprintf("An IP (%v) does not exist in the data structure!", data.ip))
        }
        if (*data_map)[data.ip].count != data.count {
            t.Error(fmt.Sprintf("An IP does not have the expected count (expected: %v, got: %v)", data.count, (*data_map)[data.ip].count))
        }
        if (*data_map)[data.ip].Latitude != data.lat {
            t.Error(fmt.Sprintf("An IP has the incorrect latitude (expected: %v, got %v)", data.lat, (*data_map)[data.ip].Latitude))
        }
        if (*data_map)[data.ip].Longitude != data.lon {
            t.Error(fmt.Sprintf("An IP has the incorrect longitude (expected: %v, got %v)", data.lon, (*data_map)[data.ip].Longitude))
        }
        if (*data_map)[data.ip].High_temp != data.temp {
            t.Error(fmt.Sprintf("An IP has the incorrect temperature (expected: %v, got %v)", data.temp, (*data_map)[data.ip].High_temp))
        }
    }
}

func populate_data(d *test_data, done chan<- bool) {
    data_map := getDataMap()
    for i := 0; i < d.count; i++ {
        data_map.addIp(d.ip)
    }
    data_map.setGeoLoc(d.ip, d.lat, d.lon)
    data_map.setHighTemp(d.ip, d.temp, false)
    done <- true
}
