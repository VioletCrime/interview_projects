package cwhutil

import (
    "sync"
    "time"
)

type ip_entry struct {
    count int

    Latitude float64
    Longitude float64
    Geo_data_fetched time.Time

    High_temp int
    Temp_data_fetched time.Time
}

type data_map map[string]*ip_entry  // Specifically, map[ip string]*ip_entry

const zeroFaren = -460

var map_instance *data_map
var once sync.Once
var map_lock = sync.RWMutex{}

// Singleton
func getDataMap() *data_map {
    once.Do(func() {
        map_instance = &data_map{}
    })
    return map_instance
}

//  =====  SETTERS  =============================================================

func (ipm data_map) addIp(ip string) {
    map_lock.Lock()
    defer map_lock.Unlock()
    _, exists := ipm[ip]
    if exists {
        ipm[ip].count += 1
    } else {
        ipm[ip] = &ip_entry{1, 0, 0, time.Unix(0, 0), zeroFaren, time.Unix(0, 0)}
    }
}

func (ipm data_map) setGeoLoc(ip string, lat float64, lon float64) {
    map_lock.Lock()
    defer map_lock.Unlock()
    ipm[ip].Latitude = lat
    ipm[ip].Longitude = lon
    ipm[ip].Geo_data_fetched = time.Now()
}

func (ipm data_map) setHighTemp(ip string, hitemp int, tempfaked bool) {
    map_lock.Lock()
    defer map_lock.Unlock()
    ipm[ip].High_temp = hitemp
    if !tempfaked {
        ipm[ip].Temp_data_fetched = time.Now()
    }
}

//  =====  GETTERS  =============================================================

func runOnMapEntry(ip string, f func(i *ip_entry) interface{} ) interface{} {
    datamap := getDataMap()
    map_lock.RLock()
    defer map_lock.RUnlock()
    return f((*datamap)[ip])
}

func getLatitude(ip string) float64 {
    return runOnMapEntry(ip, func(i *ip_entry) interface{} { return i.Latitude }).(float64)
}

func getLongitude(ip string) float64 {
    return runOnMapEntry(ip, func(i *ip_entry) interface{} {return i.Longitude}).(float64)
}

func getLatLon(ip string) (float64, float64) {
    return getLatitude(ip), getLongitude(ip)
}

func getTemp(ip string) int {
    return runOnMapEntry(ip, func(i *ip_entry) interface{} { return i.High_temp }).(int)
}

func getCount(ip string) int {
    return runOnMapEntry(ip, func(i *ip_entry) interface{} { return i.count }).(int)
}

func getGeoTimestamp(ip string) time.Time {
    return runOnMapEntry(ip, func(i *ip_entry) interface{} { return i.Geo_data_fetched }).(time.Time)
}

func getTempTimestamp(ip string) time.Time {
    return runOnMapEntry(ip, func(i *ip_entry) interface{} { return i.Temp_data_fetched }).(time.Time)
}

