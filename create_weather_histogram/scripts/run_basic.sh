#!/bin/bash

set -e

readonly PROGNAME=$(basename $0)
readonly PROGDIR=$(readlink -m $(dirname $0))
readonly ARGS="$@"

pushd .
cd $PROGDIR/../cwhutil
go install

cd $PROGDIR/../
go run CreateWeatherHistogram.go --file $PROGDIR/../logs/devops_coding_input_log1.log -outfile lol.out -buckets 10 -cache

popd

