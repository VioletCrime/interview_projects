$active_str = net users guest | findstr "active"
$guest_disabled = $active_str -match "No"
if ($guest_disabled) {
    Write-Host "Enabling the Guest Account!"
    net user guest /active:yes
} else {
    Write-Host "Disabling the Guest Account!"
    net user guest /active:no
}
net users guest | findstr "active"