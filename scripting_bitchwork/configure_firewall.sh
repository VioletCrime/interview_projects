#!/bin/bash

set -e

# If UFW isn't installed, install it
if ! which ufw > /dev/null 2>&1 ; then
    sudo apt-get install ufw
fi

sudo ufw default allow outgoing
sudo ufw default deny incoming
sudo ufw allow 443

if ufw status | grep "inactive" > /dev/null 2>&1 ; then
    sudo ufw enable
fi
